package task_for_yury;

public class Main {

    private static final int MIN = 10;
    private static int MAX = 200;

    private static final int MIN_BASKETS = 1;
    private static final int MAX_BASKETS = 10;
    private static final int TANK = (int) (Math.random() * ++MAX) + MIN;
    private static int tmp = 1;
    private static int decision = 0;
    private static int baskets;

    public static void main(String[] args) {

        System.out.println("We have tank with " + TANK + "L");
        System.out.println();
        for (baskets = MIN_BASKETS; baskets <= MAX_BASKETS; baskets++) {
            if (MAX_BASKETS % baskets == 0) {
                if (baskets == MIN_BASKETS) {
                    System.out.println(TANK + " by " + MIN_BASKETS + "L");
                    System.out.println();
                    decision++;
                } else if (baskets <= TANK) {
                    getWayDecision(TANK, baskets);
                    tmp = 1;
                    System.out.println();
                }
            }
        }
        System.out.println("We have " + decision + " way decision");
    }

    static void getWayDecision(int tank, int bask) {
        if ((tank - bask) > 0) {
            System.out.print(tank - bask + " by " + MIN_BASKETS + "L");
            System.out.println(" + " + tmp++ + " by " + baskets + "L");
            decision++;
            getWayDecision(tank, bask + baskets);
        } else if ((tank - bask) >= 0 && (tank % baskets) == 0) {
            System.out.println(tank / baskets + " by " + baskets + "L");
            decision++;
        }
    }
}
